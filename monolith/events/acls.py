from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    payload = {
        "query": f"{city}, {state}",
        "per_page": 1,
    }

    response = requests.get(url, params=payload, headers=headers)

    photo_dict = json.loads(response.content)
    return photo_dict["photos"][0]["url"]


def get_weather_data(city, state):
    loc_url = "http://api.openweathermap.org/geo/1.0/direct?"
    loc_params = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    loc_response = requests.get(loc_url, params=loc_params)

    location = json.loads(loc_response.content)
    latitude = location[0]["lat"]
    longitude = location[0]["lon"]

    weather_url = "https://api.openweathermap.org/data/2.5/weather?"
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_response = requests.get(weather_url, params=weather_params)
    weather = json.loads(weather_response.content)
    return {
        "temperature": weather["main"]["temp"],
        "description": weather["weather"][0]["description"],
    }
